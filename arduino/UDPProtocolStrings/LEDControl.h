void allLedsOff() {
  Serial.println("allLedsOff");
  for (int ledIndex = 0; ledIndex < Dot_NUMPIXELS; ledIndex++) {
    Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(0,0,0,0));
    Neo_pixels_cold.setPixelColor(ledIndex, Neo_pixels_cold.Color(0,0,0,0));
    Dot_Star.setPixelColor(ledIndex, 0,0,0); // 'On' pixel at head
  }
  Neo_pixels_warm.show();   // Send the updated pixel colors to the hardware.
  Neo_pixels_cold.show();   // Send the updated pixel colors to the hardware.
  Dot_Star.show();                     // Refresh Dot_Star
}

void allLedsOnRGB() {
  Serial.println("allLedsOnRGB");
}

void allLedsOnWhite() {
  Serial.println("allLedsOnWhite");
}

void allLedsBrightRGB(String data[]) {
  Serial.print("allLedsBrightRGB: brightness=");
  int bright = data[1].toInt();
  Serial.println(bright);
}

void allLedsBrightWhite(String data[]) {
  Serial.print("allLedsBrightWhite: brightness=");
  int bright = data[1].toInt();
  Serial.println(bright);
}

void allLedsRGB(String data[]) {
  Serial.print("allLedsRGB: ");
  int red = data[1].toInt();
  int green = data[2].toInt();
  int blue = data[3].toInt();
  Serial.print("r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.println(blue);
}

void allLedsRGBW(String data[]) {
  Serial.print("allLedsRGBW: ");
  int red = data[1].toInt();
  int green = data[2].toInt();
  int blue = data[3].toInt();
  int white = data[4].toInt();
  Serial.print("r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.print(blue);
  Serial.print(", w=");
  Serial.println(white);
}





void ledOff(String data[]) {
  Serial.print("ledOff: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.println(ledIndex);
}

void ledOnRGB(String data[]) {
  Serial.print("ledOnRGB: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.println(ledIndex);
}

void ledOnWhite(String data[]) {
  Serial.print("ledOnWhite: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.println(ledIndex);
}


void ledFadeRGB(String data[]) {
  Serial.print("ledFadeRGB: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.print(ledIndex);
  int brightness = data[2].toInt();
  Serial.print(", brightness=");
  Serial.println(brightness);
}


void ledFadeWhite(String data[]) {
  Serial.print("ledFadeWhite: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.print(ledIndex);
  int brightness = data[2].toInt();
  Serial.print(", brightness=");
  Serial.println(brightness);
}

void ledRGB(String data[]) {
  int ledIndex = data[1].toInt();
  int red = data[2].toInt();
  int green = data[3].toInt();
  int blue = data[4].toInt();
  Serial.print("ledRGB: ledIndex=");
  Serial.print(ledIndex);
  Serial.print(", r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.println(blue);
}

void ledRGBW(String data[]) {
  int ledIndex = data[1].toInt();
  int red = data[2].toInt();
  int green = data[3].toInt();
  int blue = data[4].toInt();
  int white = data[5].toInt();
  float intensity = 0.5;
  Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(intensity * red, intensity * green, intensity * blue, intensity * white));
  Neo_pixels_cold.setPixelColor(ledIndex, Neo_pixels_cold.Color(intensity * red, intensity * green, intensity * blue,  intensity * white));
  Dot_Star.setPixelColor(ledIndex, intensity * red, intensity * green, intensity * blue); // 'On' pixel at head
  Neo_pixels_warm.show();   // Send the updated pixel colors to the hardware.
  Neo_pixels_cold.show();   // Send the updated pixel colors to the hardware.
  Dot_Star.show();                     // Refresh Dot_Star
  Serial.print("ledRGBW: ledIndex=");
  Serial.print(ledIndex);
  Serial.print(", r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.print(blue);
  Serial.print(", w=");
  Serial.println(white);
}


